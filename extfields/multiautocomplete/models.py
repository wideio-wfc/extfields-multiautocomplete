# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import datetime
from django.core.urlresolvers import reverse

from wioframework.amodels import *
import wioframework.fields as models
from wioframework import decorators as dec
from wioframework import widgets
from wioframework import gen2

from django.conf import settings

MODELS = []


@wideio_owned()
@wideiomodel
class MultiAutoCompleteVote(models.Model):
    vote_for = models.ForeignKey('MultiAutoCompleteValue')
    class WIDEIO_Meta:
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING=1
        permissions = dec.perm_write_for_staff_only
        MOCKS={
            'default':[
                {
                'vote_for':'multiautocompletevalue-0000'
                }
            ]
        }


@wideio_publishable()
@wideio_timestamped
@wideiomodel
class MultiAutoCompleteValue(models.Model):
    field_name = models.CharField(max_length=128,db_index=True)
    value = models.TextField(max_length=1024,db_index=True,default="")
    widget_add_unknown = True

    def __unicode__(self):
        return unicode(self.value.decode('utf-8')) if self.value else ""

    def get_all_references(self):
        return []

    def total_uses():
        return self.multiautocomplete_set.all().count()

    def total_votes():
        return reduce(lambda x,y:x+y.priority,self.multiautocomplete_set.all(),0)    

    class WIDEIO_Meta:
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING=1
        permissions = dec.perm_write_for_staff_only
        allow_query = ['field_name']
        MOCKS={
            'default':[
                {
                'field_name':'name'
                }
            ]
        }



@wideio_publishable()
@wideio_owned()
@wideio_timestamped
@wideiomodel
class MultiAutoComplete(models.Model):
    priority = models.IntegerField(default=0)
    field_name = models.CharField(max_length=128)
    content = models.ForeignKey(MultiAutoCompleteValue, default=True)
    gpk_pk = models.TextField('object ID', null=True)
    generic_pk = gen2.GenericForeignKey2(
        fk_field="gpk_pk")

    def on_add(self, request):
        return {'_redirect': self.get_view_url()}

    def get_all_references(self):
        return [self.content]

    class WIDEIO_Meta:
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING=1
        permissions = dec.perm_write_for_logged_users_only
        allow_search = [
            'gpk_type',
            'content__icontains',
            'content_istartswith']

        MOCKS={
            'default':[
                {
                'field_name':'name'
                }
            ]
        }

        class Actions:

            @wideio_action(
                icon="icon-thumbs-down",
                possible=lambda o,
                r: (
                    (r.user is not None) and (
                        r.user.is_authenticated()) and (
                        MultiAutoCompleteVote.objects.filter(
                            owner=r.user,
                            vote_for_id=o.id).count() != 0)))
            def vote_down(self, request):
                # FIXME: VOTING DOWN SHOULD ONLY BE POSSIBLE IF VOTE NOT BEEN
                # EXPRESSED BEFORE
                MultiAutoCompleteVote.objects.filter(
                    owner=request.user,
                    vote_for_id=self.id).delete()
                self.priority = self.priority - 1
                self.save()
                return ""

            @wideio_action(
                icon="icon-thumbs-up",
                possible=lambda o,
                r: (
                    (r.user is not None) and (
                        r.user.is_authenticated()) and (
                        MultiAutoCompleteVote.objects.filter(
                            owner=r.user,
                            vote_for_id=o.id).count() == 0)))
            def vote_up(self, request):
                # FIXME: VOTING SHOULD ONLY BE POSSIBLE IF VOTE HAS NOT BEEN
                # EXPRESSED BEFORE
                rpv = MultiAutoCompleteVote()
                rpv.owner = request.user
                rpv.vote_for_id = self.id
                rpv.save()
                self.priority = self.priority + 1
                self.save()
                return ""

MODELS.append(MultiAutoCompleteValue)
MODELS += get_dependent_models(MultiAutoCompleteValue)

MODELS.append(MultiAutoComplete)
MODELS += get_dependent_models(MultiAutoComplete)

MODELS.append(MultiAutoCompleteVote)
MODELS += get_dependent_models(MultiAutoCompleteVote)
