# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import datetime
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
#from django.db import models


import wioframework.fields as models
from wioframework.amodels import *
from wioframework import decorators as dec

import settings
from django.conf import settings
from functools import reduce


def lazyimport(m, v, n, _self):
    def lip():
        xm = m
        if (not hasattr(_self, n)):
            xm = __import__(m, fromlist=xm.split(".")[:-1])
            r = getattr(xm, v)
            setattr(_self, n, r)
        else:
            r = getattr(_self, n)
        return r
    return lip


class MultiAutoCompleteField():

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs

    def get_all_references(self, _self):
        return list(set(reduce(lambda x, y: x + [y] + y.get_all_references(), self.MultiAutoComplete(
        ).objects.filter(gpk_pk=_self.pk, field_name=self.field_name), [])))

    def contribute_to_class(self, cls, field_name):
        self.MultiAutoComplete = lazyimport(
            "extfields.multiautocomplete.models",
            "MultiAutoComplete",
            "_MultiAutoComplete",
            self)
        self.MultiAutoCompleteValue = lazyimport(
            "extfields.multiautocomplete.models",
            "MultiAutoCompleteValue",
            "_MultiAutoCompleteValue",
            self)

        self.model = cls
        self.field_name = field_name
        self.name = field_name
        cls._meta.virtual_fields.append(self)
        # cls._meta.add_field(self)

        def add_paragraph_url(_self):
            import urllib
            return self.MultiAutoComplete().get_add_url() + "?" + \
                urllib.urlencode({"field_name": self.field_name,
                                  "priority": 0,
                                  "gpk_pk": _self.pk,
                                  "gpk_type": ""})

        def add_paragraph_url1(_self):
            return self.MultiAutoComplete().get_add_url()

        def add_paragraph_url2(_self):
            return {"field_name": field_name, "priority": 0, "gpk_pk": _self.pk, "gpk_type": "", "_AJAX": 1,"_INVOKE_EXTRA_CONTENT": self.field_name}

        def getter(_self):
            return (self.MultiAutoComplete(), self.MultiAutoComplete().objects.filter(gpk_pk=_self.pk, field_name=field_name).order_by("-priority"),
                    {"add_url": add_paragraph_url,
                     "add_url_part1": add_paragraph_url1,
                     "add_url_part2": add_paragraph_url2})

        def setter(_self, nv):
            #raise NotImplemented
            from wioframework import wiotext
            getter(_self)[1].delete()
            if type(nv) in [str, unicode]:
                np = self.MultiAutoCompleteValue().objects.filter(
                    field_name=field_name,
                    value=nv)
                if np.count()==0:
                    np=self.MultiAutoCompleteValue()()
                    np.field_name=field_name
                    np.value=nv
                    np.save()
                    np={"content":np}
                else:
                    np=[{"content":np[0]}]
            if type(nv) in [list, tuple] and len(
                    nv) and type(nv[0]) in [str, unicode]:
                rnp=[]
                for cnv in nv:
                    np = self.MultiAutoCompleteValue().objects.filter(
                        field_name=field_name,
                        value=cnv)
                    if np.count()==0:
                        np=self.MultiAutoCompleteValue()()
                        np.field_name=field_name
                        np.value=cnv
                        np.save()
                        np={"content":np}
                    else:
                        np={"content":np[0]}
                    rnp.append(np)
                np=rnp
            for e in np:
                print e
                ce= self.MultiAutoComplete()(
                    generic_pk=_self,
                    field_name=field_name,
                    **e)
                ce.save()
        setattr(cls, field_name, property(getter, setter))
